package com.ataberkdinc.tarlamuygulama;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class TarlaDetay extends Activity {
	Button b1,b2;
	TextView t1,t2,t3,t4,t5,t6;
	int id;
	DatabaseReference database;//Firebase

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tarla_detay);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Tarlam");

		b1 = (Button)findViewById(R.id.button1);
		b2 = (Button)findViewById(R.id.button2);

		t1 = (TextView)findViewById(R.id.adi);
		t2 = (TextView)findViewById(R.id.buyuklugu);
		t3 = (TextView)findViewById(R.id.ili);
		t4 = (TextView)findViewById(R.id.ilcesi);
		t5 = (TextView)findViewById(R.id.ekimi);
		t6 = (TextView)findViewById(R.id.hasati) ;

		Intent intent=getIntent();
		id = intent.getIntExtra("id", 0);//id degerini integer olarak aldik. Burdaki 0 eger deger alinmazsa default olrak verilecek deger.

		Database db = new Database(getApplicationContext());
		HashMap<String, String> map = db.tarlaDetay(id);//Bu id li row un degerini hashmap e aldik.

		t1.setText(map.get("tarla_adi"));
		t2.setText(map.get("buyuk").toString());
		t3.setText(map.get("il").toString());
		t4.setText(map.get("ilce").toString());
		t5.setText(map.get("ekim").toString());
		t6.setText(map.get("hasat").toString());


		b1.setOnClickListener(new View.OnClickListener() {//Tarla duzenle

			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), TarlaDuzenle.class);
				intent.putExtra("id", (int)id);
				startActivity(intent);
			}
		});

		b2.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(TarlaDetay.this);
				alertDialog.setTitle("Uyari");
				alertDialog.setMessage("Tarla silinsin mi?");
				alertDialog.setPositiveButton("Evet", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) {
						Database db = new Database(getApplicationContext());
						db.tarlaSil(id);
						Toast.makeText(getApplicationContext(), "Tarla basariyla silindi.", Toast.LENGTH_LONG).show();
						Intent intent = new Intent(getApplicationContext(), MainActivity.class);
						startActivity(intent);//bu id li tarlayi sildik.
						finish();

					}
				});
				alertDialog.setNegativeButton("Hayir", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) {

					}
				});
				alertDialog.show();

			}
		});


	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			default: return super.onOptionsItemSelected(item);
		}
	}


}

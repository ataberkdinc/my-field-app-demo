package com.ataberkdinc.tarlamuygulama;

/**
 * Created by ataberkdinc on 8.12.2017.
 */

public class Tarlalar {

    String tarlaId;
    String tarlaAdi;
    String tarlaBuyuk;
    String tarlaIl;
    String tarlaIlce;
    String tarlaEkim;
    String tarlaHasat;


    public String getTarlaId() {
        return tarlaId;
    }

    public String getTarlaAdi() {
        return tarlaAdi;
    }

    public String getTarlaBuyuk() {
        return tarlaBuyuk;
    }

    public String getTarlaIl() {
        return tarlaIl;
    }

    public String getTarlaIlce() {
        return tarlaIlce;
    }

    public String getTarlaEkim() {
        return tarlaEkim;
    }

    public String getTarlaHasat() {
        return tarlaHasat;
    }

    public Tarlalar(String tarlaId, String tarlaAdi, String tarlaBuyuk, String tarlaIl, String tarlaIlce, String tarlaEkim, String tarlaHasat) {

        this.tarlaId = tarlaId;
        this.tarlaAdi = tarlaAdi;
        this.tarlaBuyuk = tarlaBuyuk;
        this.tarlaIl = tarlaIl;
        this.tarlaIlce = tarlaIlce;
        this.tarlaEkim = tarlaEkim;
        this.tarlaHasat = tarlaHasat;

    }

    public Tarlalar()
    {

    }
}

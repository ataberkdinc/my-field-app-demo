package com.ataberkdinc.tarlamuygulama;

import java.util.ArrayList;
import java.util.HashMap;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "tarlam_database";//database adi
	private static final String TABLE_NAME = "tarla_listesi";

	private static String TARLA_ADI = "tarla_adi";
	private static String TARLA_ID = "id";
	private static String TARLA_BUYUK = "buyuk";
	private static String TARLA_IL = "il";
	private static String TARLA_ILCE = "ilce";
	private static String TARLA_EKIM = "ekim";
	private static String TARLA_HASAT = "hasat";

	public Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}


	@Override
	public void onCreate(SQLiteDatabase db) {  // Databesi olusturuyoruz.
		String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
				+ TARLA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ TARLA_ADI + " TEXT,"
				+ TARLA_BUYUK + " TEXT,"
				+ TARLA_IL + " TEXT,"
				+ TARLA_ILCE + " TEXT,"
				+ TARLA_EKIM + " TEXT,"
				+ TARLA_HASAT + " TEXT" + ")";
		db.execSQL(CREATE_TABLE);
	}


	public void tarlaSil(int id){ //id si belli olan row u silmek icin

		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_NAME, TARLA_ID + " = ?",
				new String[] { String.valueOf(id) });
		db.close();
	}

	public void tarlaEkle(String tarla_adi, String buyuk,String il,String ilce,String ekim,String hasat) {
		//tarlaEkle methodu ise veritabanina veri eklemek icin.
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(TARLA_ADI, tarla_adi);
		values.put(TARLA_BUYUK, buyuk);
		values.put(TARLA_IL, il);
		values.put(TARLA_ILCE, ilce);
		values.put(TARLA_EKIM, ekim);
		values.put(TARLA_HASAT, hasat);

		db.insert(TABLE_NAME, null, values);
		db.close(); //Database baglantisini kapattik.
	}


	public HashMap<String, String> tarlaDetay(int id){
		//Databeseden id si belli olan rowu cekmek icin.
		//HashMap bir cift boyutlu arraydir. anahtar-deger ikililerini bir arada tutmak icin tasarlanmistir.

		HashMap<String,String> tarla = new HashMap<String,String>();
		String selectQuery = "SELECT * FROM " + TABLE_NAME+ " WHERE id="+id;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		cursor.moveToFirst();
		if(cursor.getCount() > 0){
			tarla.put(TARLA_ADI, cursor.getString(1));
			tarla.put(TARLA_BUYUK, cursor.getString(2));
			tarla.put(TARLA_IL, cursor.getString(3));
			tarla.put(TARLA_ILCE, cursor.getString(4));
			tarla.put(TARLA_EKIM, cursor.getString(5));
			tarla.put(TARLA_HASAT, cursor.getString(6));
		}
		cursor.close();
		db.close();

		return tarla;
	}

	public ArrayList<HashMap<String, String>> tarlalar(){

		//Bu methodda ise tablomuzdaki tum degerleri aliyoruz.
		//ArrayList'de hasmapleri listeliyoruz.
		//Herbir satiri deger ve value ile hashmap'e atiyoruz.
		//olusturdugumuz tum hasmapleri ArrayList'e atip geri donduruyoruz.

		SQLiteDatabase db = this.getReadableDatabase(); //databaseimizin okunabilmesi icin.
		String selectQuery = "SELECT * FROM " + TABLE_NAME;
		Cursor cursor = db.rawQuery(selectQuery, null);
		ArrayList<HashMap<String, String>> tarlalist = new ArrayList<HashMap<String, String>>();

		if (cursor.moveToFirst()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				for(int i=0; i<cursor.getColumnCount();i++)
				{
					map.put(cursor.getColumnName(i), cursor.getString(i));
				}

				tarlalist.add(map);
			} while (cursor.moveToNext());
		}
		db.close();

		return tarlalist;
	}

	public void tarlaDuzenle(String tarla_adi, String buyuk,String il,String ilce,String ekim,String hasat,int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		//Bu methodumuzda var olan verimizi guncelliyoruz.
		ContentValues values = new ContentValues();
		values.put(TARLA_ADI, tarla_adi);
		values.put(TARLA_BUYUK, buyuk);
		values.put(TARLA_IL, il);
		values.put(TARLA_ILCE, ilce);
		values.put(TARLA_EKIM, ekim);
		values.put(TARLA_HASAT, hasat);

		db.update(TABLE_NAME, values, TARLA_ID + " = ?",
				new String[] { String.valueOf(id)});
	}


	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

}

package com.ataberkdinc.tarlamuygulama;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class TarlaEkle extends Activity {
	Button b1;
	EditText e1,e2,e3,e4,e5,e6;
	DatabaseReference database;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tarla_ekle);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Tarla Ekle");

		b1 = (Button)findViewById(R.id.button1);
		e1 = (EditText)findViewById(R.id.editText1);
		e2 = (EditText)findViewById(R.id.editText2);
		e3 = (EditText)findViewById(R.id.editText3);
		e4 = (EditText)findViewById(R.id.editText4);
		e5 = (EditText)findViewById(R.id.editText5);
		e6 = (EditText)findViewById(R.id.editText6);

		b1.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				String adi,buyuklugu,ili,ilcesi,ekimi,hasati;
				adi = e1.getText().toString();
				buyuklugu = e2.getText().toString();
				ili = e3.getText().toString();
				ilcesi = e4.getText().toString();
				ekimi = e5.getText().toString();
				hasati = e6.getText().toString();

			//Firebase data ekleme
			 database = FirebaseDatabase.getInstance().getReference("Tarlalar");
			 String id = database.push().getKey();
			 Tarlalar tarlalar = new Tarlalar(id,adi,buyuklugu,ili,ilcesi,ekimi,hasati);
			 database.child(id).setValue(tarlalar);

				if(adi.matches("") || buyuklugu.matches("") || ili.matches("") || ilcesi.matches("") || ekimi.matches("") || hasati.matches("")){
					Toast.makeText(getApplicationContext(), "Tum bilgileri eksiksiz doldurunuz", Toast.LENGTH_LONG).show();
				}else{
					Database db = new Database(getApplicationContext());
					db.tarlaEkle(adi, buyuklugu, ili, ilcesi, ekimi, hasati);
					db.close();
					Toast.makeText(getApplicationContext(), "Tarlaniz basariyla eklendi.", Toast.LENGTH_LONG).show();

					e1.setText("");
					e2.setText("");
					e3.setText("");
					e4.setText("");
					e5.setText("");
					e6.setText("");
				}

			}
		});

	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			default: return super.onOptionsItemSelected(item);
		}
	}

}

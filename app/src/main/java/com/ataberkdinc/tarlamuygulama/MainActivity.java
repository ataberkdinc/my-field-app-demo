	package com.ataberkdinc.tarlamuygulama;

	import android.app.ActionBar;
	import android.content.Intent;
	import android.os.Bundle;
	import android.support.v4.app.FragmentActivity;
	import android.view.Menu;
	import android.view.MenuInflater;
	import android.view.MenuItem;
	import android.view.View;
	import android.widget.AdapterView;
	import android.widget.ArrayAdapter;
	import android.widget.ListView;
	import android.widget.Toast;
	import com.facebook.stetho.Stetho;
	import com.google.firebase.database.DataSnapshot;

	import java.util.ArrayList;
	import java.util.HashMap;

	public class MainActivity extends FragmentActivity {

		private ActionBar actionBar;
		private Menu optionsMenu;
		ListView lv;
		ArrayAdapter<String> adapter;
		ArrayList<HashMap<String, String>> tarla_liste;
		String tarla_adlari[];
		int tarla_idler[];
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_main);

			//Database Browser
			Stetho.initialize(Stetho.newInitializerBuilder(this)
					.enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
					.enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
					.build());

		}

		public void onResume()
		{
			super.onResume();
			Database db = new Database(getApplicationContext()); // Database baglantimizi olusturuyoruz.
			tarla_liste = db.tarlalar();//tarla listesini aliyoruz.
			if(tarla_liste.size()==0){
				Toast.makeText(getApplicationContext(), "Henuz tarla eklenmemis.\nYukaridaki + Butonundan Ekleyiniz", Toast.LENGTH_LONG).show();
			}else{
				tarla_adlari = new String[tarla_liste.size()]; // tarla adlarini tutucagimiz string arrayi olusturduk.
				tarla_idler = new int[tarla_liste.size()]; // tarla id lerini tutucagimiz string arrayi olusturduk.
				for(int i=0;i<tarla_liste.size();i++){
					tarla_adlari[i] = tarla_liste.get(i).get("tarla_adi");
					//tarla_lioste.get(0) bize arraylist icindeki ilk hashmap arrayini dondurur. Yani tablomuzdaki ilk satir degerlerini
					//tarla_liste.get(0).get("tarla_adi") //bize arraylist icindeki ilk hashmap arrayin anahtari tarla_adi olan value doner.

					tarla_idler[i] = Integer.parseInt(tarla_liste.get(i).get("id"));

				}
				//Tarlalari listeliyoruz ve bu listeye listener atiyoruz.
				lv = (ListView) findViewById(R.id.list_view);

				adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.tarla_adi, tarla_adlari);
				lv.setAdapter(adapter);

				lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
											long arg3) {
						//Listedeki herhangi bir yere tiklandiginda tiklanan satirin sirasini aliyoruz.
						//Bu sira id arraydeki sirayla ayni oldugundan tiklanan satirda bulunan tarlanin id sini aliyor ve tarla detaya gonderiyoruz.
						Intent intent = new Intent(getApplicationContext(), TarlaDetay.class);
						intent.putExtra("id", (int)tarla_idler[arg2]);
						startActivity(intent);

					}
				});
			}

		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			this.optionsMenu = menu;


			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.main, menu);
			return super.onCreateOptionsMenu(menu);
		}
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {

			switch (item.getItemId()) {
				case R.id.ekle:
					TarlaEkle();
					return true;
				default:
					return super.onOptionsItemSelected(item);
			}

		}

		private void TarlaEkle() {
			Intent i = new Intent(MainActivity.this, TarlaEkle.class);
			startActivity(i);
		}

	}

package com.ataberkdinc.tarlamuygulama;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;


public class TarlaDuzenle extends Activity {
	Button b1;
	EditText e1,e2,e3,e4,e5,e6;
	int id;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tarla_duzenle);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("Tarla Detay");

		b1 = (Button)findViewById(R.id.button1);
		e1 = (EditText)findViewById(R.id.editText1);
		e2 = (EditText)findViewById(R.id.editText2);
		e3 = (EditText)findViewById(R.id.editText3);
		e4 = (EditText)findViewById(R.id.editText4);
		e5 = (EditText)findViewById(R.id.editText5);
		e6 = (EditText)findViewById(R.id.editText6);

		Intent intent=getIntent();
		id = intent.getIntExtra("id", 0);

		Database db = new Database(getApplicationContext());
		HashMap<String, String> map = db.tarlaDetay(id);

		e1.setText(map.get("tarla_adi"));
		e2.setText(map.get("buyuk").toString());
		e3.setText(map.get("il").toString());
		e4.setText(map.get("ilce").toString());
		e5.setText(map.get("ekim").toString());
		e6.setText(map.get("hasat").toString());

		b1.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				String adi,buyuklugu,ili,ilcesi,ekimi,hasati;
				adi = e1.getText().toString();
				buyuklugu = e2.getText().toString();
				ili = e3.getText().toString();
				ilcesi = e4.getText().toString();
				ekimi = e5.getText().toString();
				hasati = e6.getText().toString();
				if(adi.matches("") || buyuklugu.matches("") || ili.matches("") || ilcesi.matches("") || ekimi.matches("")|| hasati.matches("") ){
					Toast.makeText(getApplicationContext(), "Tum bilgileri eksiksiz doldurunuz.", Toast.LENGTH_LONG).show();
				}else{
					Database db = new Database(getApplicationContext());
					db.tarlaDuzenle(adi, buyuklugu, ili, ilcesi,ekimi,hasati,id);//gonderdigimiz id'li tarlanin degerlerini guncelledik.
					db.close();
					Toast.makeText(getApplicationContext(), "Tarlaniz basariyla duzenlendi.", Toast.LENGTH_LONG).show();
					Intent intent = new Intent(getApplicationContext(), MainActivity.class);
					startActivity(intent);
					finish();
				}

			}
		});

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			default: return super.onOptionsItemSelected(item);
		}
	}


}

